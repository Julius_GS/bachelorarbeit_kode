# to-do: implementiere 3d-bezier mit .u-daten
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Andre Schmeißer, Fraunhofer ITWM


import numpy as np
import bezier.curve as bezier
import matplotlib.pyplot as plt
import sys

from ensreader import Ensight
from fiberreader import Fiber

def print_cfd_data(data):
    print("Coordinates:", data.coords.shape[1])
    print("Tetrahedra:",  data.tetra.shape[1]   if data.tetra   is not None else 0)
    print("Pyramids:",    data.pyramid.shape[1] if data.pyramid is not None else 0)
    print("Pentahedra:",  data.penta.shape[1]   if data.penta   is not None else 0)
    print("Hexahedra:",   data.hexa.shape[1]    if data.hexa    is not None else 0)
    for name, vals in data.scalar_vars.items():
        print(name, vals.shape)

def plot_fiber_2d(fiber):
    plt.plot(fiber.s, fiber.x, '.-', label="x")
    plt.plot(fiber.s, fiber.y, '.-', label="y")
    plt.plot(fiber.s, fiber.z, '.-', label="z")
    plt.legend()

    plt.figure()
    plt.subplot(1, 2, 1)
    plt.plot(fiber.s, fiber.u, '.-', label="speed [m/s]")
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(fiber.s, fiber.T-273.15, '.-', label="Temperature [°C]")
    plt.legend()
    plt.show()

# https://towardsdatascience.com/b%C3%A9zier-interpolation-8033e9a262c2
def get_bezier_coef(points):
    # since the formulas work given that we have n+1 points
    # then n must be this:
    n = len(points) - 1

    # build coefficents matrix
    C = 4 * np.identity(n)
    np.fill_diagonal(C[1:], 1)
    np.fill_diagonal(C[:, 1:], 1)
    C[0, 0] = 2
    C[n - 1, n - 1] = 7
    C[n - 1, n - 2] = 2

    # build points vector
    P = [2 * (2 * points[i] + points[i + 1]) for i in range(n)]
    P[0] = points[0] + 2 * points[1]
    P[n - 1] = 8 * points[n - 1] + points[n]

    # solve system, find a & b
    A = np.linalg.solve(C, P)
    B = [0] * n
    for i in range(n - 1):
        B[i] = 2 * points[i + 1] - A[i + 1]
    B[n - 1] = (A[n - 1] + points[n]) / 2

    return A, B


# returns the general Bezier cubic formula given 4 control points
def get_cubic(a, b, c, d):
    return lambda t: np.power(1 - t, 3) * a + 3 * np.power(1 - t, 2) * t * b + 3 * (1 - t) * np.power(t, 2) * c + np.power(t, 3) * d

# return one cubic curve for each consecutive points
def get_bezier_cubic(points):
    A, B = get_bezier_coef(points)
    return [
        get_cubic(points[i], A[i], B[i], points[i + 1])
        for i in range(len(points) - 1)
    ]

# evalute each cubic curve on the range [0, 1] sliced in n points
def evaluate_bezier(points, n):
    curves = get_bezier_cubic(points)
    return np.array([fun(t) for fun in curves for t in np.linspace(0, 1, n)])
    # hier kann man natürlich auch irgendwo auswerten :)

def get_bspline_coef(points,velocities):
    # nur ein platzhalter, bis ich weiß, wie die ableitungen schlussendlich
    # aussehen!
    f0x=0
    f0y=0
    f0z=0
    fnx=0
    fny=0
    fnz=0
    n = len(points) - 1
    # matrix aufbauen
    h = 1/n
    C = 4 * np.identity(n + 3)
    np.fill_diagonal(C[1:],1)
    np.fill_diagonal(C[:, 1:], 1)
    C[0,0] = -3/h
    C[0,1] = 0
    C[0,2] = 3/h
    C[n+2,n] =-3/h
    C[n+2,n+1] = 0
    C[n+2,n+2] = 3/h
    # !!!! hier könnte der fehler sein: Matrix hat Vorfaktor 1/6!
    C = 1/6 * C
    # punktevektor P = 
#    print("n =",n)
#    print(points)
#    P=np.insert([f0x, f0y, f0z],points,[fnx, fny, fnz])
    P = points
    P=np.append(P,[fnx,fny,fnz])
    P=np.append([f0x,f0y,f0z],P)
    P.shape = (n+3,3)
#    print(P)
#    print(P.shape)
#    print(C.shape)
    A = np.linalg.solve(C, P)
#    print("ermittelte koeffizienten:",A)
#    print("matrix:",C)
    return A

def hilfspsi(t):
#    return lambda t: np.power(1 - t, 3) * a + 3 * np.power(1 - t, 2) * t * b + 3 * (1 - t) * np.power(t, 2) * c + np.power(t, 3) * d
    if 0 <= t and t < 1:
        a=np.power(t,3)
    elif 1 <= t and t < 2:
        a=4-12*t+12*np.power(t,2)-3*np.power(t,3)
    elif 2 <= t and t < 3:
        a = -44+60*t-24*np.power(t,2)+3*np.power(t,3)
    elif 3 <= t and t < 4:
        a = 64-48*t+12*np.power(t,2)-np.power(t,3)
    else:
        a = 0
    return 1/6*a

def get_bspline_4(i,points):
    n = len(points) - 1
    return lambda x : hilfspsi((x-i/n)*n)
    # berechne die bezier-kurve vierten gerades mit den angegebenen
    # koeffizienten, könnte etwas umständlich so sein

def get_bspline_sum_4(A,points,x):
    n = len(points) - 1
    sum = [0, 0, 0]
    for i in range(-3,n):
        sum += A[i+3]*get_bspline_4(i,points)(x)
    return sum
    # A hat shape (n+3,3)

def evaluate_bspline_4(points,velocities,n):
    A = get_bspline_coef(points,velocities)
    return np.array([get_bspline_sum_4(A,points,x) for x in np.linspace(0,1,n)])

# def evaluate_bezier(points, n):
#     curves = get_bezier_cubic(points)
#     return np.array([fun(t) for fun in curves for t in np.linspace(0, 1, n)])



def plot_fiber_3d(fiber):
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    cm = plt.cm.plasma
    color = cm((fiber.T-min(fiber.T))/(max(fiber.T)-min(fiber.T)))
    for i in range(len(fiber.x)-1):
        ax.plot(fiber.x[i:i+2], fiber.y[i:i+2], fiber.z[i:i+2], c=color[i])
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    ax.set_zlim([0, 1])
    plt.show()

def main():
    ax = plt.axes(projection = '3d')
    if len(sys.argv) <= 1:
        cfdfilename = "../daten/cfd/cube_6x6/flow.case"
    else:
        cfdfilename = sys.argv[1]
    if len(sys.argv) <= 2:
#        fiberfilename = "../daten/fiber/fiber_cube_mixed/fiber_9.mat"
       fiberfilename = "../daten/fiber/fiber_cube_mixed_coarse_20/fiber_2.mat"
    else:
        fiberfilename = sys.argv[2]
    cfd = Ensight(cfdfilename)
#    print_cfd_data(cfd)
    fiber = Fiber(fiberfilename)
    points = np.array([fiber.x, fiber.y, fiber.z])
    points = np.transpose(points)
    velocities=fiber.der
    x, y, z = points[:,0], points[:,1], points[:,2]
#    path = evaluate_bezier(points, 50)
#    print("typ points1: ",type(points), "dim points1: ", points.shape)
#    px, py, pz = path[:,0], path[:,1], path[:,2]
    plt.figure()
#    ax.plot(px, py, pz, 'b-')
    ax.plot(x, y, z, 'ro')
#    plot_fiber_2d(fiber)
#    plot_fiber_3d(fiber)
    get_bspline_coef(points,velocities)
    # !!!!!!! das ist offensichtlich falsch! aber passt genau. wo kommt der
    # faktor 6 her??
    path = evaluate_bspline_4(points,velocities,50)
    qx, qy, qz = path[:,0], path[:,1], path[:,2]
    ax.plot(qx, qy, qz, 'g-')
    plt.show()
    print(velocities)
#    print(points)

if __name__ == '__main__':
    main()


