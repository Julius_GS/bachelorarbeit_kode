def intquadr(a,b,c,low,up):
    return a/3*(up^3-low^3)+b/2*(up^2-low^2)+c*(up-low)

def intkub(a,b,c,d,low,up):
    print(a)
    print(b)
    print(c)
    print(d)
    # soll das integral von lowerbound bis upperbound über ax^3+bx^2+cx+d
    # wiedergeben
    # das ist offenbar falsch!??? ich muss natürlich das integral über
    # |3ax^2+2bx+c| berechnen...
    integral = 0
    kritstellen = np.zeros(0)
    radikand = ((2*b)/(3*a))**2-c/(3*a)
    if radikand >= 0:
        ns1 = -(2*b)/(3*a)-sqrt(radikand)
        ns2 = -(2*b)/(3*a)+sqrt(radikand)
        if 0 <= ns1 and ns1 <= 1:
            kritstellen = np.append(kritstellen,ns1)
        if 0 <= ns2 and ns1 <= 1:
            kritstellen = np.append(kritstellen,ns2)
    if len(kritstellen) == 0:
        return intquadr(3*a,2*b,c,low,up)
    elif len(kritstellen) == 1:
        return (math.copysign(1,(low+kritstellen[0])/2)
            *intquadr(3*a,2*b,c,low,kritstellen[0])
            +math.copysign(1,(kritstellen[0]+up)/2)
            *intquadr(3*a,2*b,c,kritstellen[0],up))
        # einfach eine sign
        # function, die python nicht hat.
    elif len(kritstellen) == 2:
        int1 = intquadr(3*a,2*b,c,low,kritstellen[0])
        int2 = intquadr(3*a,2*b,c,kritstellen[0],kritstellen[1])
        int3 = intquadr(3*a,2*b,c,kritstellen[1],up)
        sign1 = math.copysign(1,(low+kritstellen[0])/2)
        sign2 = math.copysign(1,(kritstellen[0]+kritstellen[1])/2)
        sign3 = math.copysign(1,(kritstellen[1]+up)/2)
        return sign1*int1+sign2*int2+sign3*int3

def intbezierpol(a,b,c,d,low,up):
    # hier muss ein wegintegral 1. art berechnet werden, nicht das hier! hier
    # wird nicht berücksichtigt, dass a,b,c,d dreidimensional sind.
    return intkub(-a+3*b-3*c+d,3*a-6*b+3*c,-3*a+3*b,a,low,up)

def polwechsel(n,p1,p2):
    # berechne für p1<p2 in [0,1] die dazwischenliegenden stellen, an denen
    # das polyonm bei n punkten (n-1 kurven)  wechselt. so geschrieben, damit der kode später
    # leichter zu verallgemeinern ist.
    wechsellower = int(np.ceil(p1*n)/n)
    wechselupper = int(np.floor(p2*n)/n)
    return wechsellower, wechselupper

def findgridintersection_multiple_bsplines(A):
    print(test)
    # das ist noch sehr problematisch, weil ich noch keine methoden habe, um
    # auf die b-spline-gleichungen in einem intervall zuzugreifen, da müsste
    # ich symbolisch irgendeine summe auswerten

def bezierintegralcalculator(points,intlower,intupper):
    #hier irgendeine funktion, die numerisch integriert über so eine kubische
    # funktion
    n=len(points)
    A, B = get_bezier_coef(points)
    wechsellower, wechselupper = polwechsel(len(points),intlower,intupper)
    # bei wechsellower startet die kurve, bei wechselupper-1 endet sie
    a1 = points[wechsellower]
    b1 = A[wechsellower]
    c1 = B[wechsellower]
    d1 = points[wechselupper+1]
    a2 = points[wechselupper+1]
    b2 = A[wechselupper+1]
    c2 = B[wechselupper+1]
    d2 = points[wechselupper+2]

    if wechsellower > wechselupper:
        # wenn wechsellower > wechselupper: fall 1, also ist das gitter so fein,
        # dass das polynom zw. den aufeinanderfolgenden gitterschnittpunkten nicht
        # wechselt. in diesem fall genügt es, das integral über ein polynom
        # berechnen zu können
        return intbezierpol(a1,b1,c1,d1,(n*intlower)%1,n*intupper%1)
    elif wechsellower == wechselupper:
        # wenn wechsellower = wechselupper: fall 2, genau ein polynomwechsel findet
        # zwischen zwei aufeinanderfolgenden gitterschnittpunkten statt.
        return intbezierpol(a1,b1,c1,d1,(n*intlower)%1,1)+ intbezierpol(a2,b2,c2,d2,0,(n*intupper)%1)
    else: # also wechsellower < wechselupper
        sum = 0
        sum += intbezierpol(a1,b1,c1,d1,(n*intlower)%1,1)
        for j in range(wechsellower, wechselupper):
            a=points[j]
            b=A[j]
            c=B[j]
            d=points[j+1]
            sum += intberzierpol(a,b,c,d,0,1)
        sum += intberzierpol(a2,b2,c2,d2,0,(n*intupper)%1)
      # wenn wechsellower < wechselupper: fall 3, zwei oder mehr polynomwechsel
      # finden zwischen zwei aufeinanderfolgenden gitterschnittpunkten statt
