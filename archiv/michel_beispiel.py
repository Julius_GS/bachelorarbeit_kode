import matplotlib.pyplot as plt
import numpy as np
q=np.array([
     [0.166667, 0.333333, 0.333333],
     [0.      , 0.5     , 0.166667],
     [0.      , 0.666667, 0.166667],
     [0.166667, 0.5     , 0.333333],
     [0.166667, 0.166667, 0.      ],
     [0.      , 0.5     , 0.      ],
     [0.      , 0.666667, 0.      ],
     [0.166667, 0.333333, 0.      ]])
print(q.shape)
ax2 = plt.axes(projection = '3d')
ax2.plot(q[:,0],q[:,1],q[:,2],'ro')
plt.show()
