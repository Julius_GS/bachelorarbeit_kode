#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Andre Schmeißer, Fraunhofer ITWM


"""
Just a demo for how to read the files.
"""
import numpy as np
import matplotlib.pyplot as plt
import sys

from ensreader import Ensight
from fiberreader import Fiber

def print_cfd_data(data):
    print("Coordinates:", data.coords.shape[1])
    print("Tetrahedra:",  data.tetra.shape[1]   if data.tetra   is not None else 0)
    print("Pyramids:",    data.pyramid.shape[1] if data.pyramid is not None else 0)
    print("Pentahedra:",  data.penta.shape[1]   if data.penta   is not None else 0)
    print("Hexahedra:",   data.hexa.shape[1]    if data.hexa    is not None else 0)
    for name, vals in data.scalar_vars.items():
        print(name, vals.shape)
    for name, vals in data.vector_vars.items():
        print(name, vals.shape)

def plot_fiber_2d(fiber):
    plt.plot(fiber.s, fiber.x, '.-', label="x")
    plt.plot(fiber.s, fiber.y, '.-', label="y")
    plt.plot(fiber.s, fiber.z, '.-', label="z")
    plt.legend()

    plt.figure()
    plt.subplot(1, 2, 1)
    plt.plot(fiber.s, fiber.u, '.-', label="speed [m/s]")
    plt.legend()
    plt.subplot(1, 2, 2)
    plt.plot(fiber.s, fiber.T-273.15, '.-', label="Temperature [°C]")
    plt.legend()

    plt.figure()
    plt.plot(fiber.s, fiber.dxds, '.-', label="dx/ds")
    plt.plot(fiber.s, fiber.dyds, '.-', label="dy/ds")
    plt.plot(fiber.s, fiber.dzds, '.-', label="dz/ds")
    plt.legend()
    plt.show()

def plot_fiber_3d(fiber):
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    cm = plt.cm.plasma
    color = cm((fiber.T-min(fiber.T))/(max(fiber.T)-min(fiber.T)))
    for i in range(len(fiber.x)-1):
        ax.plot(fiber.x[i:i+2], fiber.y[i:i+2], fiber.z[i:i+2], c=color[i])
    ax.set_xlim([0, 1])
    ax.set_ylim([0, 1])
    ax.set_zlim([0, 1])
    plt.show()
    

def main():
    if len(sys.argv) <= 1:
        cfdfilename = "../daten/cfd/cube_6x6/flow.case"
    else:
        cfdfilename = sys.argv[1]
    if len(sys.argv) <= 2:
        fiberfilename = "../daten/fiber/fiber_cube_mixed_coarse_20/fiber_2.mat"
    else:
        fiberfilename = sys.argv[2]
    cfd = Ensight(cfdfilename)
    print_cfd_data(cfd)
    fiber = Fiber(fiberfilename)
    plot_fiber_2d(fiber)
    plot_fiber_3d(fiber)

if __name__ == '__main__':
    main()

