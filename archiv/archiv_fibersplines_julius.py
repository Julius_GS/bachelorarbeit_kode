###############
#  B-Splines  #
###############

def get_bspline_coef(points,velocities):
    n = len(points) - 1
    # Matrix aufbauen
    h = 1/n
    C = 4 * np.identity(n + 3)
    np.fill_diagonal(C[1:],1)
    np.fill_diagonal(C[:, 1:], 1)
    C[0,0] = -3/h
    C[0,1] = 0
    C[0,2] = 3/h
    C[n+2,n] =-3/h
    C[n+2,n+1] = 0
    C[n+2,n+2] = 3/h
    C = 1/6 * C
    # Aufbau Vektor R.S.
    P = points
    P=np.append(P,velocities[:,n-1])
    P=np.append(velocities[:,0],P)
    P.shape = (n+3,3)
    # ermittelte Koffizienten:
    A = np.linalg.solve(C, P)
    return A

def hilfspsi(t):
    if 0 <= t and t < 1:
        a=np.power(t,3)
    elif 1 <= t and t < 2:
        a=4-12*t+12*np.power(t,2)-3*np.power(t,3)
    elif 2 <= t and t < 3:
        a = -44+60*t-24*np.power(t,2)+3*np.power(t,3)
    elif 3 <= t and t < 4:
        a = 64-48*t+12*np.power(t,2)-np.power(t,3)
    else:
        a = 0
    return 1/6*a

def get_bspline_4(i,points):
    n = len(points) - 1
    return lambda x : hilfspsi((x-i/n)*n)
    # berechne die bezier-kurve vierten gerades mit den angegebenen
    # koeffizienten, könnte etwas umständlich so sein

def evaluate_bspline_sum_4(points,velocities,x):
    A = get_bspline_coef(points,velocities)
    n = len(points) - 1
    sum = [0, 0, 0]
    for i in range(-3,n):
        sum += A[i+3]*get_bspline_4(i,points)(x)
    return sum
    # A hat shape (n+3,3)

def evaluate_bspline_4_vector(points,velocities,feinheit): # das feinheit bezeichnet hier einfach
    # die feinheit, mit der ausgewertet wird
#    A = get_bspline_coef(points,velocities)
    return np.array([evaluate_bspline_sum_4(points,velocities,x) for x in
        np.linspace(0,1,feinheit)])





    get_bspline_coef(points,velocities)
    bsplinepath = evaluate_bspline_4_vector(points,velocities,500)
    qx, qy, qz = bsplinepath[:,0], bsplinepath[:,1], bsplinepath[:,2]
    finepoints = np.transpose(np.array([finefiber.x, finefiber.y,
        finefiber.z]))
    # b-spline
    plt.figure(1)
    ax1 = plt.axes(projection = '3d')
    # plt.axes(projection = '3d').plot(x, y, z, 'ro')
    # plt.axes(projection = '3d').plot(qx, qy, qz, 'g-')
    ax1.plot(x,y,z,'ro')
    ax1.plot(qx,qy,qz,'b-')
#    plt.title("Interpolation mit B-Splines der Ordnung 4")
    plt.savefig("bspline_plot_1.eps",dpi=300)
    #plt.show()





    # differenz b-splines
    plt.figure(3)
    bsplinediff = np.zeros(len(finepoints))
    for j in range(len(finepoints)):
        bsplinediff[j]=np.linalg.norm(bsplinepath[j,:]-finepoints[j,:])
    plt.plot(bsplinediff,'b-')
#    plt.title("Fehler bei der Interpolation mit B-Splines")
    plt.savefig("bspline_fehler_1.eps",dpi=300)
