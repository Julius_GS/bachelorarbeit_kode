#!/usr/bin/env python3
# Autor: Julius Busse
# -*- coding: utf-8 -*-
# from ensreader import *
from bezier_methoden import *
from eigenesgittergenerieren import *
from sympy.solvers import solve
from sympy import Symbol
from sympy import sin
from sympy.abc import x
from sympy import nsolve
from fiberreader import *
import numpy as np

# cfdfilename = "../daten/cfd/cube_6x6/flow.case"
# coords, tet, pyr, penta, hexa, scalar_vars, vector_vars = read_ensight(cfdfilename)
# print(coords)
# Das Problem ist am eigenen Gitter gelöst, da das in cfdfilename liegende
# Gitter keine ebenen Flächen hat.

def binichinquaderj(gitter,j,p):
    start, xax, yax, zax, achsen = gitter
    n = achsen.shape[0]
    prel = p - start
    A = np.transpose(np.array((xax, yax, zax)))
    lambdavar = np.linalg.solve(A, prel)
    jz = int(j % n)
    zhilfe = (j-jz)/n
    jy = int(zhilfe % n)
    yhilfe = (zhilfe-jy)/n
    jx = int(yhilfe % n)
    if (achsen[jx,0] <= lambdavar[0] and lambdavar[0] <= achsen[jx+1,0]
            and achsen[jy,1] <= lambdavar[1] and lambdavar[1] <= achsen[jy+1,1]
            and achsen[jz,2] <= lambdavar[2] and lambdavar[2] <= achsen[jz+1,2]):
        return 1
    return 0

def inwelchemquadersindwir(gitter, p):
    start, xax, yax, zax, achsen = gitter
    n = achsen.shape[0]
    n = (n-1)**3
    for j in range(n+1):
        if binichinquaderj(gitter, j, p):
            return j

def nachbarfinder(nges,a): # n: Gesamtanzahl Quader (kubische Zahl), j: Quaderindex
    nachbarn = np.array((a))
    n = np.round(nges**(1/3))
    # Wir müssen alle 27-1 Kombinationen erfassen:
    for i in range(-1,2):
        for j in range(-1,2):
            for k in range(-1,2):
                nachbarn = np.append(nachbarn,a+i*1+j*(n-1)+k*(n-1)**2)
    nachbarn = np.sort(nachbarn)
    nachbarn = nachbarn[nachbarn >= 0]
    nachbarn = nachbarn[nachbarn < nges]
    nachbarn = [int(a) for a in nachbarn]
    return nachbarn

def parallelepipedgitter(fiber,finefiber,gitter,randbed, feinheit):
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, randbed)
    temp = fiber.T
    thck = fiber.diam
    kurve = evaluate_bezier_vector(points,feinheit,ableitung,randbed)
    A = np.empty((feinheit,4))
    index = 0
    letzterquader = 0
    for p in kurve:
        A[index,0:3] = p
        if binichinquaderj(gitter,letzterquader,p):
            A[index, 3] = letzterquader
            print("Ich bleibe im selben Gitterelement")
        else:
            nachbarerfolg = 0
            nachbarliste = nachbarfinder(gitter.shape[0],letzterquader)
            for nachbar in nachbarliste:
                if binichinquaderj(gitter,nachbar,p):
                    A[index, 3] = nachbar
                    letzterquader = nachbar
                    nachbarerfolg = 1
                    print("Mein neues Gitterelement wurde numerisch\
                            effizient gefunden")
            if not nachbarerfolg:
                print(inwelchemquadersindwir(gitter,p))
                A[index,3] = inwelchemquadersindwir(gitter,p)
                print(A[index,3])
                letzterquader = int(A[index,3])
                print("Mein neues Gitterelement wurde nur durch\
                        Brute-Force gefunden :(")
        index += 1
    return A

def parallelepipedgitter_punkte_in_element(A):
    relevante_gitterelemente = sorted(set(A[:,3].astype(int)))
    B = np.zeros((2,0)) for gitterindex in relevante_gitterelemente:
        res = sum(1 for i in A[:,3] if i == gitterindex)
        B = np.append(B, np.array([gitterindex, res]))
    B.shape = [-1, 2]
    print(B)
    return B
