#!/usr/bin/env python3
# Autor: Julius Busse
# -*- coding: utf-8 -*-
import numpy as np
import sympy as sp
from sympy import *
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import matplotlib as matplotlib
import sys
import math

from ensreader import Ensight
from fiberreader import Fiber
############
#  Bezier  #
############

# Diese Funktion löst das LGS für die Koeffizienten von Bezier-Kurven
def get_bezier_coef(points, ableitung, randbed):
    # j=0,...,n, also:
    n = len(points) - 1

    # Matrix aufbauen und
    # Vektor auf der R.S. erstellen
    C = 4 * np.identity(n)
    np.fill_diagonal(C[1:], 1)
    np.fill_diagonal(C[:, 1:], 1)
    P = [2 * (2 * points[i] + points[i + 1]) for i in range(n)]
    if randbed == 'nat':
        C[0, 0] = 2
        C[n - 1, n - 1] = 7
        C[n - 1, n - 2] = 2
        P[0] = points[0] + 2 * points[1]
        P[n - 1] = 8 * points[n - 1] + points[n]
    elif randbed == 'hermite':
        C[0, 0] = 3
        C[0, 1] = 0
        C[n-1, n-2] = 3
        C[n-1, n-1] = 12
        P[0] = 3*points[0] + 1/n*ableitung[0,:]
        P[n - 1] = -1/n*ableitung[n,:] + 3*points[n] + 12*points[n-1]
    else:
        raise ValueError("unzulässige randbedbedingungen")

    # Lösen und Punkte b rekonstruieren
    A = np.linalg.solve(C, P)
    if points.ndim == 2:
        B = np.zeros((n,3))
    else:
        B = np.zeros(n)
    for i in range(n - 1):
        B[i] = 2 * points[i + 1] - A[i + 1]
    B[n - 1] = A[n-2]+4*A[n-1]-4*points[n-1]

    return A, B

def get_cubic(a, b, c, d):
    # Gibt die allgemeine Gleichung eines kubischen Bezier-Polynoms wieder
    return lambda t: ((1-t)**3*a+3*(1-t)**2*t*b+3*(1-t)*t**2*c+t**3*d)

def get_cubic_normableitung(a, b, c, d):
    return lambda t : np.linalg.norm((t**2)*3*(-a+3*b-3*c+d) +
            t*2*(3*a-6*b+3*c)+(-3*a+3*b))

def get_bezier_cubic(points, ableitung, randbed):
    # Löst nach Koeff. und gibt Vektor mit jedem einzelnen Bezier-Polynom aus
    A, B = get_bezier_coef(points, ableitung, randbed)
    return [
        get_cubic(points[i], A[i], B[i], points[i + 1])
        for i in range(len(points) - 1)
    ]

def get_bezier_cubic_quick(points,A,B):
    return [
        get_cubic(points[i], A[i], B[i], points[i + 1])
        for i in range(len(points) - 1)
    ]

def evaluate_bezier_quick(x,points,A,B):
    curves = get_bezier_cubic_quick(points,A,B)
    n = len(curves)
    if (not isinstance(x,float)):
        return np.array((0,0,0))
        print("ich habe es geschafft")
    else:
        if x < 1:
            return curves[int(np.floor(n*x))]((n*x)%1)
        else:
            return curves[len(curves)-1](1)

def get_bezier_cubic_normableitung(points, ableitung, randbed):
    # Löst nach Koeff und gibt Vektor mit jeder einzelnen
    # Normableitung aus
    A, B = get_bezier_coef(points, ableitung, randbed)
    return [
        get_cubic_normableitung(points[i], A[i], B[i], points[i+1])
        for i in range(len(points) - 1)
    ]

def evaluate_bezier(x,points, ableitung, randbed):
    # Wertet jedes Teilpolynom auf [0,1] aus und hängt alles aneinander
    # => Bezier-KURVE
    A, B = get_bezier_coef(points, ableitung, randbed)
    curves = get_bezier_cubic_quick(points,A,B)
    n = len(curves)
    if x < 1:
        return curves[int(n*x-((n*x)%1))]((n*x)%1)
    else:
        return curves[len(curves)-1](1)

def evaluate_bezier_normabl(x,points,ableitung, randbed):
    # Wertet die Normableitung an einem bestimmten Punkt aus
    curves = get_bezier_cubic_normableitung(points, ableitung, randbed)
    n = len(curves)
    if x < 1:
        return n*curves[int((n*x)-((n*x)%1))]((n*x)%1)
        # Der Faktor n ist NICHT völlig willkürlich, sondern kommt aus der
        # Kettenregel.
    else:
        return n*curves[len(curves)-1](1)

def evaluate_bezier_vector(points, feinheit, ableitung, randbed):
    # Wertet die Bezier-Kurve an n äquidistanten Punkten aus
    # n: An wie vielen Punkten soll ausgewertet werden
    # Etwa: n=500, len(curves)=20
    A,B = get_bezier_coef(points, ableitung, randbed)
    C=np.zeros((feinheit,3))
    for m in range(feinheit):
        t=m/(feinheit-1)
        C[m,:]=evaluate_bezier_quick(t,points,A,B)
    return C

def evaluate_bezier_normabl_vector(points, feinheit, ableitung, randbed):
    # Wertet die Bezier-Normableitung an n äquidistanten Punkten
    # aus
    A=np.zeros(0)
    for m in range(feinheit):
        t=m/(feinheit-1)
        A = np.append(A, evaluate_bezier_normabl(t, points, ableitung, randbed))
    return A

def bezier_initializor(fiber, finefiber, randbed):
    feinheit = len(finefiber.x)
    points = np.transpose(np.array([fiber.x, fiber.y, fiber.z]))
    ableitung=np.transpose(np.array([fiber.dxds, fiber.dyds, fiber.dzds]))
    x, y, z = points[:,0], points[:,1], points[:,2]
    bezierpath = evaluate_bezier_vector(points, feinheit, ableitung, randbed)
    px, py, pz = bezierpath[:,0], bezierpath[:,1], bezierpath[:,2]
    finepoints = np.transpose(np.array([finefiber.x, finefiber.y,
        finefiber.z]))
    return points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints

##############################################################################
#                            Alle Plotter ab hier                            #
##############################################################################

def finefiber_plot(fiber, finefiber, randbed):
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, randbed)
    plt.figure(42)
    finefiberpoints = np.transpose(np.array([finefiber.x, finefiber.y,
        finefiber.z]))
    qx, qy, qz = finefiberpoints[:,0], finefiberpoints[:,1], finefiberpoints[:,2]
    ax3 = plt.axes(projection = '3d')
    ax3.plot(qx,qy,qz,'ro')
    ax3.set_xlabel('[m]')
    ax3.set_ylabel('[m]')
    ax3.set_zlabel('[m]')
    plt.savefig("../../TeX/figures/finefiber_plot_new.eps",dpi=300)

def bezier_interp_plot(fiber, finefiber):
    #######################################
    #  Plottet die interpolierende Kurve  #
    #######################################
    points, ableitung, x, y, z, bezierpath, pxnat, pynat, pznat, finepoints = \
            bezier_initializor(fiber, finefiber, 'nat')
    points, ableitung, x, y, z, bezierpath, pxherm, pyherm, pzherm, finepoints = \
            bezier_initializor(fiber, finefiber, 'hermite')
    plt.figure(40)
    ax2 = plt.axes(projection = '3d')
    ax2.plot(x,y,z,'ro')
    ax2.plot(pxnat,pynat,pznat,'g-')
    ax2.plot(pxherm,pyherm,pzherm,'b-')
    ax2.plot(pxherm[0],pyherm[0],pzherm[0],marker = 's', markerfacecolor = 'none',
            markeredgewidth = 2, markeredgecolor = 'orange',linestyle = 'dashed', markersize
            = 20)
    ax2.set_xlabel('[m]')
    ax2.set_ylabel('[m]')
    ax2.set_zlabel('[m]')
    plt.savefig("../../TeX/figures/bezier_plot_new.eps",dpi=300)

def bezier_mit_temp(fiber, finefiber, randbed):
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, randbed)
    feinheit = len(finefiber.x)
    temp = evaluate_bezier_vector(fiber.T,feinheit, ableitung, 'nat')
    cmap = plt.cm.hot
    norm = plt.Normalize(vmin = np.min(temp), vmax =
            np.max(temp))
    color = cmap(norm(temp[:,0]))
    print(color.shape)
    print(px.shape)
    plt.figure(2)
    ax4 = plt.axes(projection = '3d')
    for j in range(len(px)-2):
        ax4.plot(px[j:j+2],py[j:j+2],pz[j:j+2],'-',c=color[j,:])
    ax4.set_xlabel('[m]')
    ax4.set_ylabel('[m]')
    ax4.set_zlabel('[m]')
    sm = plt.cm.ScalarMappable(cmap = cmap, norm = norm)
    plt.colorbar(sm, label="[°K]",pad=0.12)
    plt.savefig("../../TeX/figures/bezier_mit_temp_new.eps",dpi=300)

def bezier_mit_dicke(fiber, finefiber, randbed):
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, randbed)
    feinheit = len(finefiber.x)
    thck = evaluate_bezier_vector(fiber.diam,feinheit, ableitung, 'nat')
    cmap = plt.cm.viridis
    norm = plt.Normalize(vmin = np.min(thck), vmax =
            np.max(thck))
    color = cmap(norm(thck[:,0]))
    plt.figure(900)
    ax5 = plt.axes(projection = '3d')
    for j in range(len(px)-2):
        ax5.plot(px[j:j+2],py[j:j+2],pz[j:j+2],'-',c=color[j,:])
    ax5.set_xlabel('[m]')
    ax5.set_ylabel('[m]')
    ax5.set_zlabel('[m]')
    sm = plt.cm.ScalarMappable(cmap = cmap, norm = norm)
    plt.colorbar(sm, label="[m]", pad=0.12)
    plt.savefig("../../TeX/figures/bezier_mit_dicke_new.eps",dpi=300)

def bezier_fehler_plot(fiber, finefiber):
    ###########################################################################
    #  Plottet die Differenz zwischen der Interpolation und der feinen Kurve  #
    ###########################################################################
    points, ableitung, x, y, z, bezierpathnat, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, 'nat')
    points, ableitung, x, y, z, bezierpathherm, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, 'hermite')
    feinheit = len(finefiber.x)
    fig, ax = plt.subplots()
    bezierdiffnat = np.zeros(len(finepoints))
    bezierdiffherm = np.zeros(len(finepoints))
    for j in range(len(finepoints)):
        bezierdiffnat[j]=np.linalg.norm(bezierpathnat[j,:]-finepoints[j,:])
        bezierdiffherm[j]=np.linalg.norm(bezierpathherm[j,:]-finepoints[j,:])
    plt.plot(np.linspace(0,1,feinheit),bezierdiffnat,'g-')
    plt.plot(np.linspace(0,1,feinheit),bezierdiffherm,'b-')
    ax.set_xlabel('$s$')
    ax.set_ylabel('[m]')
    plt.grid()
    plt.savefig("../../TeX/figures/bezier_fehler_new.eps",dpi=300)

def bezier_normabl_plot(fiber, finefiber):
    ###########################################
    #  Plottet die Normableitung des Splines  #
    ###########################################
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, 'hermite')
    feinheit = len(finefiber.x)
    fig, ax = plt.subplots()
    beziernormablnat = evaluate_bezier_normabl_vector(points,feinheit, ableitung,
            'nat')
    beziernormablherm = evaluate_bezier_normabl_vector(points,feinheit, ableitung,
            'hermite')
    plt.plot(np.linspace(0,1,feinheit),beziernormablnat,'g-')
    plt.plot(np.linspace(0,1,feinheit),beziernormablherm,'b-')
    ax.set_xlabel('$s$')
    ax.set_ylabel('')
    plt.grid()
    plt.savefig("../../TeX/figures/bezier_normabl_new.eps",dpi=300)

def bezier_temp_plot(fiber, finefiber, randbed):
    ##############################################
    #  plottet die Interpolation der Temperatur  #
    ##############################################
    if randbed == 'hermite':
        raise ValueError('Bitte für das eindimensionale Zeug natürliche\
        Randbedingungen nutzen')
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, randbed)
    feinheit = len(finefiber.x)
    fig, ax = plt.subplots()
    temp = evaluate_bezier_vector(fiber.T,feinheit, ableitung, randbed)
    plt.plot(np.linspace(0,1,feinheit),temp, 'g-')
    ax.set_xlabel('$s$')
    ax.set_ylabel('[°K]')
    plt.grid()
    plt.savefig("../../TeX/figures/bezier_temp_new.eps",dpi=300)

def bezier_thck_plot(fiber, finefiber, randbed):
    #########################################
    #  Plottet die Interpolation der Dicke  #
    #########################################
    if randbed == 'hermite':
        raise ValueError('Bitte für das eindimensionale Zeug natürliche\
        Randbedingungen nutzen')
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, randbed)
    feinheit = len(finefiber.x)
    fig, ax = plt.subplots()
    thck = evaluate_bezier_vector(fiber.diam,feinheit, ableitung, randbed)
    plt.plot(np.linspace(0,1,feinheit),thck, 'g-')
    ax.set_xlabel('$s$')
    ax.set_ylabel('[m]')
    plt.tight_layout()
    plt.grid()
    plt.savefig("../../TeX/figures/bezier_thck_new.eps",dpi=300)

def linke_randbedingungen_visualisieren(fiber, finefiber):
    #######################################################################
    #  Zeigt den Unterschied zwischen den Randbedingungen am linken Rand  #
    #######################################################################
    randbed = 'nat'
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, randbed)
    feinheit = len(finefiber.x)
    plt.figure(2)
    ax7 = plt.axes(projection = '3d')
    hermitekurve = evaluate_bezier_vector(points, feinheit, ableitung, 'hermite')
    natkurve = evaluate_bezier_vector(points, feinheit, ableitung, 'nat')
    ax7.plot(natkurve[0:28,0], natkurve[0:28,1], natkurve[0:28,2], 'g-')
    ax7.plot(hermitekurve[0:28,0], hermitekurve[0:28,1], hermitekurve[0:28,2], 'b-')
    ax7.view_init(-20,-29)
    ax7.plot(points[0:2,0], points[0:2,1], points[0:2,2], 'ro')
    ax7.set_xticklabels([])
    ax7.set_yticklabels([])
    ax7.set_zticklabels([])
    plt.tight_layout()
    plt.savefig("../../TeX/figures/linke_randbedingungen_new.eps",dpi=300)
