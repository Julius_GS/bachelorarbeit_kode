#!/usr/bin/env python3
# Autor: Julius Busse
# -*- coding: utf-8 -*-

# Diese Datei soll ein eigenes Gitter von Quadern oder Parallelepipeden
# generieren.
# Drei Punkte werden benötigt, um die Orientierung im Raum anzugeben
# dann teile ich jede Achse zufällig in Teilabschnitte auf.
import numpy as np
import matplotlib.pyplot as plt
import random as random

def gittergenerator(n):
    start = np.array((-0.1,-0.1,-0.1)) # Gitter startet im Ursprung und wird am ende
    # um "start" verschoben
    xachse =np.array((1.5,-0.05,0))
    yachse = np.array((0,1.4,0))
    zachse = np.array((0,0,1.2))
    achsen = np.empty((n,3))
    for dim in range(3):
        for j in range(n-2):
            achsen[j,dim] = random.random()
            achsen[n-2,dim] = 0
            achsen[n-1,dim] = 1
        achsen[:,dim] = np.sort(achsen[:,dim])
    # gitter = np.empty(((n-1)**3,3,4))# Anzahl Quader, Dimensionen, Punkte pro
    # # Gitterelement
    # index = 0
    # for j in range(n-1):
    #     for i in range(n-1):
    #         for k in range(n-1):
    #             gitter[index,:,0] = (xachse * achsen[j,0]   + yachse *
    #                     achsen[i,1]   + zachse * achsen[k,2] + start)
    #             gitter[index,:,1] = (xachse * achsen[j+1,0] + yachse *
    #                     achsen[i,1]   + zachse * achsen[k,2] + start)
    #             gitter[index,:,2] = (xachse * achsen[j,0]   + yachse *
    #                     achsen[i+1,1] + zachse * achsen[k,2] + start)
    #             gitter[index,:,3] = (xachse * achsen[j,0]   + yachse *
    #                     achsen[i,1]   + zachse * achsen[k+1,2] + start)
    #             index += 1
    # return gitter # (n-1)^3 x 3 x 4 array
    return start, xachse, yachse, zachse, achsen

def main():
    gitter = gittergenerator(10)
    ax2 = plt.axes(projection = '3d')
    ax2.plot(gitter[0,0,:], gitter[0,1,:], gitter[0,2,:],'ro')
    plt.show()

if __name__ == '__main__':
    main()
