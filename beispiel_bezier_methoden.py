#!/usr/bin/env python3
# Autor: Julius Busse
# -*- coding: utf-8 -*-
import numpy as np
import sympy as sp
from sympy import *
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import sys
import math

from ensreader import Ensight
from fiberreader import Fiber
from fiberreader import add_diameter_to_fiber_object
from bezier_methoden import *

def main():
    if len(sys.argv) <= 1:
        cfdfilename = "../daten/cfd/cube_6x6/flow.case"
    else:
        cfdfilename = sys.argv[1]
    if len(sys.argv) <= 2:
       fiberfilename = "../daten/fiber/fiber_cube_mixed_coarse_20/fiber_2.mat"
       finefiberfilename = "../daten/fiber/fiber_cube_mixed/fiber_2.mat"
    else:
        fiberfilename = sys.argv[2]
    cfd = Ensight(cfdfilename)
    fiber = Fiber(fiberfilename)
    finefiber = Fiber(finefiberfilename)
    # finefiber_plot(fiber, finefiber, 'nat')
    # bezier_interp_plot(fiber, finefiber)
    # bezier_fehler_plot(fiber, finefiber)
    # bezier_normabl_plot(fiber, finefiber)
    # bezier_temp_plot(fiber, finefiber, 'nat')
    # bezier_thck_plot(fiber, finefiber, 'nat')
    # bezier_mit_temp(fiber, finefiber, 'hermite')
    # bezier_mit_dicke(fiber, finefiber, 'hermite')
    linke_randbedingungen_visualisieren(fiber, finefiber)
    plt.show()

if __name__ == '__main__':
    main()
