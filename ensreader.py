#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Andre Schmeißer, Fraunhofer ITWM

"""
This is a simple reader for EnSight files, which ignores most features and makes a
lot of assumptions:
- Files are in ASCII format
- Files contain only a single time step (stationary).
- Only scalar per node / vector per node values supported
- While reading, all parts are merged into one. It is assumed the variable files contain
  all parts in the same order as the model file.
"""

import numpy as np
import pathlib

class Ensight:
    def __init__(self, filename):
        self.coords, self.tetra, self.pyramid, self.penta, self.hexa, self.scalar_vars, \
        self.vector_vars = read_ensight(filename)


def read_case_file(filename):
    """Read the .case file of an EnSight data set"""
    model_file = None
    scalar_vars, vector_vars = {}, {}
    with open(filename) as f:
        lines = f.readlines()
    lines  = [l.strip() for l in lines]
    if lines[0] != "FORMAT":
        raise IOError("Invalid file format")
    for l in lines:
        if ":" in l:
            key, value = l.split(":")
            if key == "model":
                model_file = value.strip()
            elif key == "scalar per node":
                name, file = value.split()
                scalar_vars[name] = file
            elif key == "vector per node":
                name, file = value.split()
                vector_vars[name] = file
    return (model_file, scalar_vars, vector_vars)


def read_scalar_values(f, num_values):
    """Read num_values scalars from a file"""
    vals = np.zeros(num_values)
    for idx in range(num_values):
        vals[idx] = float(f.readline())
    return vals

def read_vector_values(f, num_values):
    """Read num_values coordinates from a file"""
    vals = np.zeros((3, num_values))
    for dim in range(3):
        for idx in range(num_values):
            vals[dim, idx] = float(f.readline())
    return vals

def read_coordinates(f):
    """Read a list of coordinates from a model file"""
    num_coords = int(f.readline())
    return read_vector_values(f, num_coords)

def read_cells(f, num_vertices):
    """Read a list of cell indices from a model file"""
    num_cells = int(f.readline())
    cells = np.zeros((num_vertices, num_cells), dtype=int)
    for idx in range(num_cells):
        cells[:, idx] = [int(s) for s in f.readline().split()]
    return cells

def concat(arrays):
    """Concatenate numpy arrays"""
    if len(arrays) == 0:
        return None
    dim = len(arrays[0].shape)
    return np.concatenate(arrays, dim-1)

def read_model_file(filename):
    """Read the model file (*.geo) of EnSight data set"""
    coords = []
    tetra_cells, pyramid_cells, penta_cells, hexa_cells = [], [], [], []
    cur_part_idx, cur_part_name = None, None
    part_map = {}
    with open(filename) as f:
        line = f.readline()
        while line:
            line = line.strip()
            if line == "part":
                cur_part_idx = int(f.readline())
                cur_part_name = f.readline()
            elif line == "coordinates":
                c = read_coordinates(f)
                num_coords = c.shape[1]
                part_map[cur_part_idx] = (num_coords, cur_part_name)
                coords.append(c)
            elif line == "tetra4":
                tetra_cells.append(read_cells(f, 4))
            elif line == "pyramid5":
                pyramid_cells.append(read_cells(f, 5))
            elif line == "penta6":
                penta_cells.append(read_cells(f, 6))
            elif line == "hexa8":
                hexa_cells.append(read_cells(f, 8))
            line = f.readline()
    return part_map, concat(coords), \
           concat(tetra_cells), \
           concat(pyramid_cells), \
           concat(penta_cells), \
           concat(hexa_cells)


def read_scalar_var_file(filename, part_map):
    """Read a scalar variable file of EnSight data set"""
    cur_part_idx = None
    values = []
    with open(filename) as f:
        line = f.readline()
        while line:
            line = line.strip()
            if line == "part":
                cur_part_idx = int(f.readline())
            elif line == "coordinates":
                v = read_scalar_values(f, part_map[cur_part_idx][0])
                values.append(v)
            line = f.readline()
    return concat(values)

def read_vector_var_file(filename, part_map):
    """Read a scalar variable file of EnSight data set"""
    cur_part_idx = None
    values = []
    with open(filename) as f:
        line = f.readline()
        while line:
            line = line.strip()
            if line == "part":
                cur_part_idx = int(f.readline())
            elif line == "coordinates":
                v = read_vector_values(f, part_map[cur_part_idx][0])
                values.append(v)
            line = f.readline()
    return concat(values)

def read_ensight(filename):
    """Read the Ensight data set for the given *.case / *.encas file name"""
    case_file = pathlib.Path(filename)
    model_file, scalar_var_files, vector_var_files = read_case_file(case_file)
    part_map, coords, tet, pyr, penta, hexa = \
        read_model_file(case_file.parent / model_file)
    scalar_vars, vector_vars = {}, {}
    for name, file in scalar_var_files.items():
        scalar_vars[name] = read_scalar_var_file(case_file.parent / file, part_map)
    for name, file in vector_var_files.items():
        vector_vars[name] = read_vector_var_file(case_file.parent / file, part_map)
    return coords, tet, pyr, penta, hexa, scalar_vars, vector_vars
