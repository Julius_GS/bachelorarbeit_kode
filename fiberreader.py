#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Andre Schmeißer, Fraunhofer ITWM
import numpy as np

"""
Reads the data of a fiber simulation.

The fiber is represented by its position in space, i.e. the 3D curve
r(s) = (x(s), y(s), z(s)) for s in [0,1], as well as several attached state
variables, e.g. speed u(s), temperature T(s).
The data is given in on a discretized grid s_i, x_i = x(s_i), y_i = y(s_i), ... 
along with derivatives dx_i = dx/ds (s_i), dy_i = dy/ds(s_i), ...

The input file contains the grid as a vector as well as the coordinates/state
variables and their derivatives as matrices. For ease of use, these are unpacked
and given more convenient names
"""
def compute_diameter(fiber, rho, dIn, uIn, TIn):
    rhoIn = rho(TIn)
    return np.sqrt(rhoIn / rho(fiber.T) * uIn / fiber.u * dIn*dIn)

def add_diameter_to_fiber_object(fiber):
    dIn = 2e-4
    uIn = 1
    TIn = 200 + 273.15
    def rho(T):
        x0, x1 = 20 + 273.15, 220 + 273.15
        y0, y1 = 900, 750
        return (T-x0)/(x1-x0) * y1 + (x1-T)/(x1-x0) * y0
    fiber.diam = compute_diameter(fiber, rho, dIn, uIn, TIn)

class Fiber:
    def __init__(self, filename):
        from scipy.io import loadmat
        d = loadmat(filename)
        self.grid = d["grid"]
        self.sol = d["solution"]
        self.der = d["derivative"]

        # curve parameter
        self.s = self.grid
        # coordinates
        self.r = self.sol[:3, :]
        self.x, self.y, self.z = self.r[0, :],  self.r[1, :],  self.r[2, :]
        # speed
        self.u = self.sol[10, :]
        # temperature
        self.T = self.sol[17, :]
        add_diameter_to_fiber_object(self)
        # Data contains some further variables, ignored here
        
        # derivatives of coordinate with respect to s
        self.drds = self.der[:3, :]
        self.dxds, self.dyds, self.dzds = self.drds[0, :],  self.drds[1, :],  self.drds[2, :]
        # derivatives of variables with respect to s
        self.duds = self.der[10, :]
        self.dTds = self.der[17, :]
