#!/usr/bin/env python3
# Autor: Julius Busse
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import sys

from ensreader import Ensight
from fiberreader import Fiber

############
#  Bezier  #
############


#von towardsdatascience dings ??? quelle
# Diese Funktion löst das LGS für die Koeffizienten von Bezier-Kurven
def get_bezier_coef(points):
    # j=0,...,n, also:
    n = len(points) - 1

    # Matrix erstellen
    C = 4 * np.identity(n)
    np.fill_diagonal(C[1:], 1)
    np.fill_diagonal(C[:, 1:], 1)
    C[0, 0] = 2
    C[n - 1, n - 1] = 7
    C[n - 1, n - 2] = 2

    # Vektor auf der R.S. erstellen
    P = [2 * (2 * points[i] + points[i + 1]) for i in range(n)]
    P[0] = points[0] + 2 * points[1]
    P[n - 1] = 8 * points[n - 1] + points[n]

    # Lösen und Punkte b rekonstruieren
    A = np.linalg.solve(C, P)
    B = [0] * n
    for i in range(n - 1):
        B[i] = 2 * points[i + 1] - A[i + 1]
    B[n - 1] = (A[n - 1] + points[n]) / 2

    return A, B


# Gibt die allgemeine Gleichung eines kubischen Bezier-Polynoms wieder
def get_cubic(a, b, c, d):
    return lambda t: (np.power(1 - t, 3) * a + 3 * np.power(1 - t, 2) * t * b
            + 3 * (1 - t) * np.power(t, 2) * c + np.power(t, 3) * d)

# Löst nach Koeff. und gibt Vektor mit jedem einzelnen Bezier-Polynom aus
def get_bezier_cubic(points):
    A, B = get_bezier_coef(points)
    return [
        get_cubic(points[i], A[i], B[i], points[i + 1])
        for i in range(len(points) - 1)
    ]

# Wertet jedes Teilpolynom auf [0,1] aus und hängt alles aneinander
# => Bezier-KURVE
def evaluate_bezier(points, n):
    curves = get_bezier_cubic(points)
    return np.array([fun(t) for fun in curves for t in np.linspace(0, 1, n)])
    # hier kann man natürlich auch irgendwo auswerten :)

def get_bspline_coef(points,velocities):
    n = len(points) - 1
    # Matrix aufbauen
    h = 1/n
    C = 4 * np.identity(n + 3)
    np.fill_diagonal(C[1:],1)
    np.fill_diagonal(C[:, 1:], 1)
    C[0,0] = -3/h
    C[0,1] = 0
    C[0,2] = 3/h
    C[n+2,n] =-3/h
    C[n+2,n+1] = 0
    C[n+2,n+2] = 3/h
    C = 1/6 * C
    # Aufbau Vektor R.S.
    P = points
    P=np.append(P,velocities[:,n-1])
    P=np.append(velocities[:,0],P)
    P.shape = (n+3,3)
    # ermittelte Koffizienten:
    A = np.linalg.solve(C, P)
    return A

###############
#  B-Splines  #
###############

def hilfspsi(t):
    if 0 <= t and t < 1:
        a=np.power(t,3)
    elif 1 <= t and t < 2:
        a=4-12*t+12*np.power(t,2)-3*np.power(t,3)
    elif 2 <= t and t < 3:
        a = -44+60*t-24*np.power(t,2)+3*np.power(t,3)
    elif 3 <= t and t < 4:
        a = 64-48*t+12*np.power(t,2)-np.power(t,3)
    else:
        a = 0
    return 1/6*a

def get_bspline_4(i,points):
    n = len(points) - 1
    return lambda x : hilfspsi((x-i/n)*n)
    # berechne die bezier-kurve vierten gerades mit den angegebenen
    # koeffizienten, könnte etwas umständlich so sein

def get_bspline_sum_4(A,points,x):
    n = len(points) - 1
    sum = [0, 0, 0]
    for i in range(-3,n):
        sum += A[i+3]*get_bspline_4(i,points)(x)
    return sum
    # A hat shape (n+3,3)

def evaluate_bspline_4(points,velocities,n):
    A = get_bspline_coef(points,velocities)
    return np.array([get_bspline_sum_4(A,points,x) for x in np.linspace(0,1,n)])

def main():
    ax = plt.axes(projection = '3d')
    if len(sys.argv) <= 1:
        cfdfilename = "../daten/cfd/cube_6x6/flow.case"
    else:
        cfdfilename = sys.argv[1]
    if len(sys.argv) <= 2:
#        fiberfilename = "../daten/fiber/fiber_cube_mixed/fiber_9.mat"
       fiberfilename = "../daten/fiber/fiber_cube_mixed_coarse_20/fiber_2.mat"
    else:
        fiberfilename = sys.argv[2]
    cfd = Ensight(cfdfilename)
#    print_cfd_data(cfd)
    fiber = Fiber(fiberfilename)
    points = np.array([fiber.x, fiber.y, fiber.z])
    points = np.transpose(points)
    velocities=np.array([fiber.dxds, fiber.dyds, fiber.dzds])
    x, y, z = points[:,0], points[:,1], points[:,2]
    bezierpath = evaluate_bezier(points, 50)
    px, py, pz = bezierpath[:,0], bezierpath[:,1], bezierpath[:,2]
#    plt.figure()
#    plt.title("titel 2")
#    plt.show()
#    ax.plot(px, py, pz, 'b-')
#    plot_fiber_2d(fiber)
#    plot_fiber_3d(fiber)
    get_bspline_coef(points,velocities)
    bsplinepath = evaluate_bspline_4(points,velocities,100)
    qx, qy, qz = bsplinepath[:,0], bsplinepath[:,1], bsplinepath[:,2]
    # b-spline
    plt.figure(1)
    ax.plot(x, y, z, 'ro')
    ax.plot(qx, qy, qz, 'g-')
    plt.title("Interpolation mit B-Splines der Ordnung 4")
    plt.savefig("bspline_plot_1.eps",dpi=300)
    #plt.show()

    # bezier
    plt.figure(2)
    ax.plot(x,y,z, 'ro')
    ax.plot(px,py,pz, 'b-')
    plt.title("Interpolation mit kubischen Bezier-Kurven")
    plt.savefig("bezier_plot_1.eps",dpi=300)
    plt.show()

if __name__ == '__main__':
    main()
