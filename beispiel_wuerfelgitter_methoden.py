#!/usr/bin/env python3
# Autor: Julius Busse
# -*- coding: utf-8 -*-
from wuerfelgitter_methoden import *

def main():
    # Selbe Variablen wie in fibersplines
    if len(sys.argv) <= 1:
        cfdfilename = "../daten/cfd/cube_6x6/flow.case"
    else:
        cfdfilename = sys.argv[1]
    if len(sys.argv) <= 2:
       fiberfilename = "../daten/fiber/fiber_cube_mixed_coarse_20/fiber_6.mat"
       finefiberfilename = "../daten/fiber/fiber_cube_mixed/fiber_6.mat"
    else:
        fiberfilename = sys.argv[2]
    cfd = Ensight(cfdfilename)
    fiber = Fiber(fiberfilename)
    finefiber = Fiber(finefiberfilename)
    gridwidth = 0.1
    # A = wuerfelgitter(fiber,finefiber,gridwidth,'nat')
    # np.save("speicher/wuerfelgitter",A)
    A = np.load("speicher/wuerfelgitter.npy")
    wuerfelgitter_mit_fadenlaenge(fiber, finefiber, gridwidth, 'hermite', A)
    plt.show()


if __name__ == '__main__':
    main()
