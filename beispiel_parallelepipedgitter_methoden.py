#!/usr/bin/env python3
# Autor: Julius Busse
# -*- coding: utf-8 -*-
import array_to_latex as a2l
from bezier_methoden import *
from eigenesgittergenerieren import *
from sympy.solvers import solve
from sympy import Symbol
from fiberreader import *
import numpy as np
from parallelepipedgitter_methoden import *

def main():
    # Selbe Variablen wie in fibersplines
    if len(sys.argv) <= 1:
        cfdfilename = "../daten/cfd/cube_6x6/flow.case"
    else:
        cfdfilename = sys.argv[1]
    if len(sys.argv) <= 2:
        fiberfilename = "../daten/fiber/fiber_cube_mixed_coarse_20/fiber_2.mat"
        finefiberfilename = "../daten/fiber/fiber_cube_mixed/fiber_2.mat"
    else:
        fiberfilename = sys.argv[2]
    cfd = Ensight(cfdfilename)
    fiber = Fiber(fiberfilename)
    finefiber = Fiber(finefiberfilename)
    randbed = 'hermite'
    # n=20
    # gitter = gittergenerator(n)
    # np.save("speicher/gitter", gitter)
    gitter = np.load("speicher/gitter.npy",allow_pickle=True)
    # print(inwelchemquadersindwir(gitter,np.array((1,1,1))))
    print(gitter)
    # A = parallelepipedgitter(fiber,finefiber,gitter,'hermite',500)
    # np.save("speicher/parallelepipedgitter_resultat", A)
    A = np.load("speicher/parallelepipedgitter_resultat.npy")
    B = parallelepipedgitter_punkte_in_element(A)
    print(B)
    print(a2l.to_ltx(B, frmt = '{:.0f}', arraytype = 'bmatrix'))

if __name__ == '__main__':
    main()
