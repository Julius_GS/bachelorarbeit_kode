#!/usr/bin/env python3
# Autor: Julius Busse
# -*- coding: utf-8 -*-
from bezier_methoden import *
from matplotlib.colors import ListedColormap, BoundaryNorm, Normalize
import matplotlib as matplotlib
from matplotlib.collections import LineCollection
import matplotlib.cm as cm

def findcubicroots(a,b,c,d,intlower,intupper):
    # Findet Nullstellen des kubischen Polynoms ax^3+bx^2+cx+d
    # zwischen intlower und intupper
    # Arbeitet analytisch
    x = symbols('x')
    lsgmenge = solveset(Eq(a*x**3+b*x**2+c*x+d,0),x)
    index1=0
    reellelsg = np.zeros(0)
    for lsg in lsgmenge:
        # Überprüfe, ob die Lösungen reell sind und im geforderten
        # Intervall liegen
        if lsg.is_real and intlower <= lsg and lsg <= intupper:
            reellelsg = np.append(reellelsg,float(lsg))
            index1 += 1
    return(reellelsg)

def findgridintersections(a,b,c,d,gridwidth):
    # Findet Schnittstellen mit dem äquidistanten Würfelgitter mit
    # Gitterlänge gridwidth
    reellelsg = np.zeros(0)
    extremakandidaten = findmaxcubic(a,b,c,d)
    numlower = np.floor(extremakandidaten[0]/gridwidth)
    numlower = int(numlower)
    numupper = np.ceil(extremakandidaten[1]/gridwidth)
    numupper = int(numupper)
    for j in range(numlower,numupper+1):
        # Sucht nach Nullstellen von p(x)-j*gridwidth in einem
        # geeigneten Intervall
        lsglokal = findcubicroots(a,b,c,d-j*gridwidth,0,1)
        reellelsg = np.append(reellelsg,lsglokal)
    return(reellelsg)

def evalcubic(a,b,c,d,x):
    return a*x**3+b*x**2+c*x+d

def findmaxcubic(a,b,c,d):
    # Findet die Extremstellen von ax^2+bx^2+cx+d zwischen 0 und 1
    lrand = d
    rrand = a+b+c+d
    # Berechnung der Nullstellen der ersten Ableitung
    exstelle = np.array([lrand, rrand])
    radikand = ((2*b)/(3*a))**2-c/(3*a)
    if radikand >= 0:
        # Überprüft, ob die Nullstellen reell sind
        mitte1 = -b/(3*a)+sqrt(radikand)
        mitte2 = -b/(3*a)-sqrt(radikand)
        # Überprüfe die Funktionswerte an den Kandidaten für
        # Extremstellen
        if 0 <= mitte1 and mitte1 <= 1:
            polwert = a*mitte1**3+b*mitte1**2+c*mitte1+d
            exstelle = np.append(exstelle, polwert)
        if 0 <= mitte2 and mitte2 <= 1:
            polwert = a*mitte2**3+b*mitte2**2+c*mitte2+d
            exstelle = np.append(exstelle,polwert)
    maxwerte = np.zeros(0)
    for stelle in exstelle:
        maxwerte = np.append(maxwerte,evalcubic(a,b,c,d,stelle))
    maxwerte = np.sort(maxwerte)
    # maxwerte[0]: Minimum, maxwerte[n-1]: Maximum
    return (maxwerte[0], maxwerte[len(maxwerte) - 1])


def findgridintersection_multiple_bezier(points,gridwidth,ableitung,randbed):
    # Findet Gitterschnittstellen einer dreidimensionalen
    # Bezier-Kurve mit natürlichen Randbedingungen, welche points
    # interpoliert
    [A,B] = get_bezier_coef(points,ableitung, randbed)
    n = len(A)
    reellelsg = np.zeros(0)
    for dim in range(3):
        for j in range(n):
            # Iteriert die findgridintersections-Funktion
            a = points[j,dim]
            b = A[j,dim]
            c = B[j,dim]
            d = points[j+1,dim]
            lsglokal = findgridintersections(
                    -a+3*b-3*c+d,3*a-6*b+3*c,-3*a+3*b,a,gridwidth)
            lsglokal = (lsglokal+j)/n
            reellelsg = np.append(reellelsg,lsglokal)
    return(np.sort(reellelsg))

def inwelchemwuerfelsindwir(x,gridwidth):
    # Überprüft in welchem Würfel des Würfelgitters mit Gitterweite
    # gridwidth x liegt
    ecken = np.zeros(3)
    for dim in range(3):
        ecken[dim] = np.floor(x[dim]/gridwidth)
    return ecken

def bezierintegralcalculator(points,ableitung,randbed,low,up):
    # Numerische Integration
    return integrate.quad(evaluate_bezier_normabl, low, up,
            args=(points,ableitung,randbed,))

def compute_diameter(fiber, rho, dIn, uIn, TIn):
    rhoIn = rho(TIn)
    return np.sqrt(rhoIn / rho(fiber.T) * uIn / fiber.u * dIn*dIn)

def wuerfelgitter(fiber,finefiber,gridwidth,randbed):
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
            bezier_initializor(fiber, finefiber, randbed)
    temp = fiber.T
    thck = fiber.diam
    # Gibt ein Array aus, wo zu jedem Wüfel die Länge und mittlere
    # Temperatur des enthaltenen Fadens berechnet wird
    gitterschnitt = findgridintersection_multiple_bezier(points,gridwidth,
            ableitung, randbed)
    n = len(gitterschnitt)
    A = np.zeros((n,6))
    # In A schreiben wir die Ergebnisse rein
    for j in range(n-1):
        # Bestimme Koordinaten des Würfels
        wuerfelmitte = evaluate_bezier((gitterschnitt[j] +
            gitterschnitt[j+1])/2,points, ableitung, randbed)
        welcherwuerfel = \
                inwelchemwuerfelsindwir(wuerfelmitte, gridwidth)*gridwidth
        # print("Wir sind im Würfel ", welcherwuerfel)
        A[j,:3] = welcherwuerfel
        # Bestimme Länge der Kurve im Würfel
        kurvenlaenge=bezierintegralcalculator(points, ableitung, randbed, gitterschnitt[j],
                gitterschnitt[j+1])[0]
        A[j,3] = kurvenlaenge
        # print("da hat die kurve die länge ", kurvenlaenge)
        # Bestimme mittlere Termperatur der Kurve im Würfel
        temperatur = integrate.quad(evaluate_bezier, gitterschnitt[j],
                gitterschnitt[j+1], args =
                (temp,ableitung,'nat',))/(gitterschnitt[j+1]-gitterschnitt[j])
        # print(" und das mittel über die temperatur beträgt ", temperatur)
        A[j, 4] = temperatur[0]
        # Bestimme mittlere Dicke der Kurve im Würfel
        thickness = integrate.quad(evaluate_bezier, gitterschnitt[j],
                gitterschnitt[j+1], args =
                (thck,ableitung,'nat'))/(gitterschnitt[j+1]-gitterschnitt[j])
        # print(" und das mittel über die fadendicke beträgt ", thickness)
        A[j,5] = thickness[0]
    return A

def wuerfelgitter_mit_fadenlaenge(fiber, finefiber, gridwidth, randbed,
        wuerfelgitter_resultat):
    points, ableitung, x, y, z, bezierpath, px, py, pz, finepoints = \
        bezier_initializor(fiber, finefiber, randbed)
    temp = fiber.T
    thck = fiber.diam
    gitterschnitt = findgridintersection_multiple_bezier(points,gridwidth,
            ableitung, randbed)
    A, B = get_bezier_coef(points, ableitung, randbed)
    ax2 = plt.axes(projection = '3d')
    ############################
    #  länge im gitterelement  #
    ############################
    plt.figure(1)
    for j in range(len(gitterschnitt)-2):
        kurve = [evaluate_bezier_quick(x,points,A,B) for x in
                np.linspace(gitterschnitt[j],gitterschnitt[j+1],20)]
        kurve = np.array(kurve)
        cmap = cm.viridis
        norm = Normalize(vmin = np.min(wuerfelgitter_resultat[:,3]), vmax =
                np.max(wuerfelgitter_resultat[:,3]))
        color = cmap(norm(wuerfelgitter_resultat[j,3]))
        ax2.plot(kurve[:,0], kurve[:,1], kurve[:,2], color=color#,lw=5
                )
        ax2.plot(kurve[0,0], kurve[0,1], kurve[0,2], 'ro')
        # fig.colorbar(p)
    ax2.set_xlabel('[m]')
    ax2.set_ylabel('[m]')
    ax2.set_zlabel('[m]')
    sm = plt.cm.ScalarMappable(cmap = cmap, norm = norm)
    plt.colorbar(sm, label='[m]', pad=0.12)
    plt.savefig("../../TeX/figures/bezier_markierte_gitterlaenge_new.eps",dpi=300)
    # fiber von gitterschnitt[j-1] bis gitterschnitt[j] plotten, einfärben mit
    # wuerfelgitter_resultat[j,3,4 oder 5]
